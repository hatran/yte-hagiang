﻿var lat = 20.9537;
var lon = 107.0824;
var map = new L.Map('map', {

        zoom: 20,
        minZoom: 4,
});



    // create a new tile layer
    var tileUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl,
    {
        attribution: '',//'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 15
    });

    // add the layer to the map
    map.addLayer(layer);

    var cuuthuong = getParameterByName('ct');
    var cuuhoa = 1
    var chihuy = getParameterByName('chh');

    var lstGarbagePoint1 = [{
        name: 'Điểm tập kết rác thải Lán Bè',
        lat: '20.9519',
        lon: '107.0719',
        address: 'Lán Bè, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Đồng Hồ Lán Bè',
        lat: '20.95290',
        lon: '107.09945',
        address: 'Đồng Hồ, Lán Bè, Bạch Đằng, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường 25 - 4',
        lat: '20.9521',
        lon: '107.0807',
        address: 'Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải Hàng Than - Ba Đèo',
        lat: '20.95279',
        lon: '107.07323',
        address: 'Ba Đèo, Lán Bè, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Lê Lợi',
        lat: '20.9590',
        lon: '107.0749',
        address: 'Lê Lợi, Lán Bè, Yết Kiêu, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Lê Lai',
        lat: '20.9577',
        lon: '107.0838',
        address: 'Lê Lai, Lán Bè, Trần Hưng Đạo, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    }];

    var lstGarbagePoint2 = [{
        name: 'Điểm tập kết rác thải đường Lán Bè Bạch Đằng',
        lat: '20.9526',
        lon: '107.0907',
        address: 'Lán Bè, Bạch Đằng, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Võ Nguyên Giáp',
        lat: '20.95115',
        lon: '107.10317',
        address: 'Võ Nguyên Giáp, Lán Bè, Hồng Hải, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Văn Lang',
        lat: '20.95100',
        lon: '107.07809',
        address: 'Văn Lang, Lán Bè, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Bến Đoan Mỹ Gia',
        lat: '20.94859',
        lon: '107.07308',
        address: 'Khu Mỹ Gia, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Trần Thái Tông - Nguyễn Thái Học',
        lat: '20.9626',
        lon: '107.0737',
        address: 'Trần Thái Tông, Lán Bè, Yết Kiêu, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    }];

    var lstGarbagePoint3 = [{
        name: 'Điểm tập kết rác đường Giếng Đồn',
        lat: '20.95551',
        lon: '107.08674',
        address:'Giếng Đồn, Lán Bè, Trần Hưng Đạo, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác đường Dốc Học',
        lat: '20.9501',
        lon: '107.0758',
        address:'Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Nhà Thờ Hòn Gai',
        lat: '20.95319',
        lon: '107.08350',
        address: 'Nhà thờ Hòn Gai, Nhà Thờ, Lán Bè, Trần Hưng Đạo, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    },{
        name: 'Điểm tập kết rác thải đường Lam Sơn',
        lat: '20.9635',
        lon: '107.0789',
        address: 'Lam Sơn, Lán Bè, Yết Kiêu, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Namm'
    }];

    var lstBaiRac = [{
        name: 'Bãi rác Đèo Sen',
        lat: '20.99667',
        lon: '107.10846',
        address: 'Trần Phú, Lán Bè, Hà Khánh, Hạ Long, Thành phố Hạ Long, Tỉnh Quảng Ninh, Việt Nam'
    }]

    var xethang1 = [[20.9546, 107.0689], [20.9519, 107.0719], [20.95100, 107.07809], [20.9521, 107.0849], [20.95551, 107.08674]];
    var xethang2 = [[lat - 0.03, lon - 0.03], [lat + 0.03, lon - 0.02],[lat + 0.03, lon - 0.017],[lat + 0.01, lon - 0.017],[lat + 0.01, lon - 0.010],[lat, lon]];
    var xebot1 = [[lat - 0.01, lon + 0.02], [lat - 0.041, lon + 0.047], [lat - 0.041, lon + 0.047], [lat - 0.01, lon + 0.02]];
    var xebot2 = [[lat - 0.032, lon + 0.068], [lat + 0.02, lon - 0.02], [lat + 0.02, lon - 0.02], [lat - 0.032, lon + 0.068]];
    var xethang5 = [[lat - 0.003, lon - 0.005], [lat - 0.01, lon - 0.005], [lat - 0.000, lon - 0.000]];
    var xethang6 = [[lat - 0.004, lon - 0.007], [lat - 0.01, lon - 0.005], [lat - 0.000, lon - 0.000]];

    var xecuuthuong1 = [[lat - 0.005, lon + 0.005], [lat - 0.010, lon + 0.015], [lat - 0.005, lon + 0.010],[lat - 0.002, lon + 0.005], [lat, lon]];
    var xecuuthuong2 = [[lat + 0.01, lon - 0.02], [lat + 0.01, lon - 0.015],[lat + 0.00, lon - 0.010],[lat, lon]];
    // var xecuuthuong3 = [[lat - 0.07218, lon - 0.076218], [lat - 0.065009, lon - 0.053655], [lat - 0.031934, lon - 0.047476], [lat - 0.004908, lon - 0.027906], [lat - 0.002727, lon - 0.004292]];
    // var xecuuthuong4 = [[lat + 0.038468, lon - 0.156898], [lat + 0.042758, lon - 0.11879], [lat + 0.055709, lon - 0.068836], [lat + 0.032826, lon - 0.030384], [lat - 0.003216, lon - 0.00368]];

    //var mymap = [[lat, lon], [lat + 0.12, lon + 0.12],[lat - 0.12, lon - 0.12]];

    var londonBrusselFrankfurtAmsterdamLondon = [[51.507222, -0.1275], [50.85, 4.35],
    [50.116667, 8.683333], [52.366667, 4.9], [51.507222, -0.1275]];

    var barcelonePerpignanPauBordeauxMarseilleMonaco = [
        [41.385064, 2.173403],
        [42.698611, 2.895556],
        [43.3017, -0.3686],
        [44.837912, -0.579541],
        [43.296346, 5.369889],
        [43.738418, 7.424616]
    ];


//map.fitBounds(mymap);

    map.setView([lat, lon], 16);

    var thungracredIcon = L.icon({
        iconUrl: 'images/iconMaker2.png',
        iconSize: [30, 30], // size of the icon
    });

    var thungracgreenIcon = L.icon({
        iconUrl: 'images/iconMaker1.png',
        iconSize: [30, 30], // size of the icon
    });

    var thungracgyellowIcon = L.icon({
        iconUrl: 'images/iconMaker3.png',
        iconSize: [30, 30], // size of the icon
    });

    var xechoracICon = L.icon({
        iconUrl: 'images/xechorac.png',
        iconSize: [30, 30], // size of the icon
    });

    var bairacIcon = L.icon({
        iconUrl: 'images/bairac.png',
        iconSize: [30, 30], // size of the icon
    });

    // var marker = L.marker([lat, lon], { icon: FireIcon }).addTo(map);
    // 
    
    //Add multi maker
    for (var ik = 0; ik < lstGarbagePoint1.length; ik++) {
        var maker = L.marker([lstGarbagePoint1[ik].lat, lstGarbagePoint1[ik].lon], { icon: thungracredIcon }).addTo(map);
    }

    for (var ic = 0; ic < lstGarbagePoint2.length; ic++) {
        var maker = L.marker([lstGarbagePoint2[ic].lat, lstGarbagePoint2[ic].lon], { icon: thungracgreenIcon }).addTo(map);
    }

    for (var ia = 0; ia < lstGarbagePoint3.length; ia++) {
        var maker = L.marker([lstGarbagePoint3[ia].lat, lstGarbagePoint3[ia].lon], { icon: thungracgyellowIcon }).addTo(map);
    }

    for (var ix = 0; ix < lstBaiRac.length; ix++) {
        var maker = L.marker([lstBaiRac[ix].lat, lstBaiRac[ix].lon], { icon: bairacIcon }).addTo(map);
    }




    // 

    if (cuuhoa == 1) {
        //========================================================================
        var marker1 = L.Marker.movingMarker(xethang1,
            [12000, 24000, 40000], { autostart: false, loop: true, icon: xechoracICon }).addTo(map);

        marker1.loops = 0;
        marker1.bindPopup();
        marker1.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                // .setContent('Xe thang 1 thuộc: ' + lstFirefighter[0].name + '<br> Còn <strong>8</strong> phút nữa hiện trường')
                .setContent('Xe thang 1 <br> Còn <strong>8</strong> phút nữa đến hiện trường')
                .openOn(map);
        });

        marker1.once('click', function () {
            marker1.start();
            marker1.closePopup();
            marker1.unbindPopup();
            marker1.on('click', function () {
                if (marker1.isRunning()) {
                    marker1.pause();
                } else {
                    marker1.start();
                }
            });
        });

        //========================================================================

        var marker2 = L.Marker.movingMarker(xethang2,
            [50000, 40000, 40000, 50000], { autostart: false, loop: true, icon: xechoracICon }).addTo(map);

        marker2.loops = 0;
        marker2.bindPopup();
        marker2.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                // .setContent('Xe thang 2 thuộc: ' + lstFirefighter[1].name + '<br> Còn <strong>30</strong> phút nữa hiện trường')
                .setContent('Xe thang 2 <br> Còn <strong>30</strong> phút nữa đến hiện trường')
                .openOn(map);
        });
        marker2.once('click', function () {
            marker2.start();
            marker2.closePopup();
            marker2.unbindPopup();
            marker2.on('click', function () {
                if (marker2.isRunning()) {
                    marker2.pause();
                } else {
                    marker2.start();
                }
            });
        });
    }
    var red_road = new L.Routing.control({
    waypoints: [
        L.latLng(20.9519, 107.0719),        
        L.latLng(20.99667,107.10846)
    ],
    // createMarker: function(waypointIndex, waypoint, numberOfWaypoints) {
    //         return L.marker(L.latLng(10.7896, 106.6854))
    //             .bindPopup('Tuyến đường từ: ' + lstPolice[0].name + 'di chuyển thông thoáng.'+' <br>'+' Sau 8 phút có thể tới nơi');
    //     },
    addWaypoints: false,
    lineOptions: {
      styles: [{color: 'red', opacity: 1, weight: 5}]
   },
   draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);


//     var yellow_road = new L.Routing.control({
//     waypoints: [
//         L.latLng(10.7611, 106.6893),        
//         L.latLng(20.99667,107.10846)
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'yellow', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);

//     var green_road = new L.Routing.control({
//     waypoints: [
//         L.latLng(10.7568, 106.7178),        
//         L.latLng(20.99667,107.10846)
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'green', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
