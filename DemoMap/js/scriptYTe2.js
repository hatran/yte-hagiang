﻿
var lat = 20.95416;
var lon = 107.08856;
var map = new L.Map('map', {

        zoom: 18,
        minZoom: 4,
});

    // create a new tile layer
    var tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl,
    {
        attribution: '',//'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 18
    });

    map.addLayer(layer);

    map.setView([lat, lon], 16);

    var hospitalIcon = L.icon({
        iconUrl: 'images/pin-hopital.png',
        iconSize: [30, 30], // size of the icon
    });

    var hospitalMainIcon = L.icon({
        iconUrl: 'images/iconMaker2.gif',
        iconSize: [40, 40], // size of the icon
    });

    var lstHospital = data;
    var lstColor = [
  {
    "color": "#003366",
  },
  {
    "color": "#0000FF"
  },
  {
    "color": "#33CC33"
  },
  {
    "color": "#990033"
  },
  {
    "color": "#FF66FF"
  },
  {
    "color": "#CD853F"
  },
  {
    "color": "#CDCD00"
  },
  {
    "color": "#8B658B"
  }
];

    // var marker = L.marker([lat, lon], { icon: hospitalIcon }).addTo(map);
    var route = null;
    var maker = null;
    var color= null;
    for (var ik = 0; ik < lstHospital.length; ik++) {
        if(ik != 0) {
            maker = L.marker([lstHospital[ik].lat, lstHospital[ik].lon], { icon: hospitalIcon }).addTo(map);
            color = lstColor[ik-1].color;
            route = new L.Routing.control({
                waypoints: [
                    L.latLng(lstHospital[ik].lat, lstHospital[ik].lon),        
                    L.latLng(lat,lon)
                ],
                addWaypoints: false,
                lineOptions: {
                  styles: [{color: color, opacity: 1, weight: 5}]
               },
               createMarker: function() { return null; },
               draggableWaypoints: false,
                routeWhileDragging: true
            }).addTo(map);
        } else {
            var maker = L.marker([lstHospital[ik].lat, lstHospital[ik].lon], { icon: hospitalMainIcon }).addTo(map);
        }
    }

//     var pccc01 = new L.Routing.control({
//     waypoints: [
//         L.latLng(10.7896, 106.6854),        
//         L.latLng(lat,lon)
//     ],
//     createMarker: function(waypointIndex, waypoint, numberOfWaypoints) {
//             return L.marker(L.latLng(10.7896, 106.6854))
//                 .bindPopup('Tuyến đường từ: ' + lstPolice[0].name + 'di chuyển thông thoáng.'+' <br>'+' Sau 8 phút có thể tới nơi');
//         },
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'green', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);


//     var pccc02 = new L.Routing.control({
//     waypoints: [
//         L.latLng(10.7611, 106.6893),        
//         L.latLng(lat,lon)
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'red', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);

//     var pccc03 = new L.Routing.control({
//     waypoints: [
//         L.latLng(10.7568, 106.7178),        
//         L.latLng(lat,lon)
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'blue', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);
    

//     var cuuThuogn01 = new L.Routing.control({
//     waypoints: [
//         L.latLng(10.7735, 106.6796),       
//         L.latLng(lat,lon)
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'green', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);

//     var cuuThuogn02 = new L.Routing.control({
//     waypoints: [
//         L.latLng(10.7566, 106.6750),        
//         L.latLng(lat,lon)
//     ],
//     addWaypoints: false,
//     lineOptions: {
//       styles: [{color: 'red', opacity: 1, weight: 5}]
//    },
//    draggableWaypoints: false,
//     routeWhileDragging: true
// }).addTo(map);





    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    function openModal(idModal) {
        document.getElementById(idModal).style.display = "block";
    }
