﻿$(function () {

    // Create the chart
    var chart = new Highcharts.chart('container1', {
        chart: {
            type: 'column',
            events: {
                load: function () {
                    var count = 0;
                    setInterval(function () {
                        if (count == 0) {
                            chart.series[0].setData([45, 20, 35]);
                            //chart.series[1].setData([10.57]);
                            //chart.series[2].setData([7.23]);
                            count = 1;
                        }
                        else {
                            chart.series[0].setData([0, 0, 0]);
                            //chart.series[1].setData([0]);
                            //chart.series[2].setData([0]);
                            count = 0;
                        }
                    }, 2000);
                }
            }
        },
        title: {
            text: 'Biểu đồ tổng kết về tình hình PCCC trên địa bàn',
            style: {
                color: '#000000',
                //fontWeight: 'bold'
            }
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Chập điện', 'Vàng mã', 'Khí ga'],

            labels: {
                autoRotation: 0,

            }
        },
        yAxis: {
            max: 50,
            title: {
                text: '%'
            }
        },
        colors: ['green', 'red', 'blue'],

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        tooltip: {
            headerFormat: '<span style="font-size:11px"></span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
        },

        "series": [
          {
              //"name": "Browsers",
              "colorByPoint": true,
              "data": [
                {
                    "name": "Chập điện",
                    "y": 45,
                },
                {
                    "name": "Vàng mã",
                    "y": 20,
                },
                {
                    "name": "Khí ga",
                    "y": 35,
                }
              ]
          }
        ],

    });

});