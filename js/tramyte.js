var datatramyte = [
 {
   "STT": 1,
   "Name": "Trạm y tế Thị trấn Phú Mỹ",
   "address": "Thị trấn Phú Mỹ, Huyện Tân Thành, Tỉnh HÀ GIANG",
   "Longtitude": 10.5905345,
   "Latitude": 107.0480378,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 2,
   "Name": "Trạm y tế Xã Tân Hoà",
   "address": "Xã Tân Hoà, Huyện Tân Thành, Tỉnh HÀ GIANG",
   "Longtitude": 10.5330334,
   "Latitude": 107.1071,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 3,
   "Name": "Trạm y tế Xã Tân Hải",
   "address": "Xã Tân Hải, Huyện Tân Thành, Tỉnh HÀ GIANG",
   "Longtitude": 10.5005153,
   "Latitude": 107.1071,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 4,
   "Name": "Trạm y tế Xã Phước Hoà",
   "address": "Xã Phước Hoà, Huyện Tân Thành, Tỉnh HÀ GIANG",
   "Longtitude": 10.5146084,
   "Latitude": 107.0480378,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 5,
   "Name": "Trạm y tế Xã Tân Phước",
   "address": "Xã Tân Phước, Huyện Tân Thành, Tỉnh HÀ GIANG",
   "Longtitude": 10.5639613,
   "Latitude": 107.0744052,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 6,
   "Name": "Trạm y tế Xã Mỹ Xuân",
   "address": "Xã Mỹ Xuân, Huyện Tân Thành, Tỉnh HÀ GIANG",
   "Longtitude": 10.6288504,
   "Latitude": 107.0421324,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 7,
   "Name": "Trạm y tế Xã Sông Xoài",
   "address": "Xã Sông Xoài, Huyện Tân Thành, Tỉnh HÀ GIANG",
   "Longtitude": 10.6605776,
   "Latitude": 107.1543601,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 8,
   "Name": "Trạm y tế Xã Hắc Dịch",
   "address": "Xã Hắc Dịch, Huyện Tân Thành, Tỉnh HÀ GIANG",
   "Longtitude": 10.6466114,
   "Latitude": 107.113007,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 9,
   "Name": "Trạm y tế Xã Châu Pha",
   "address": "Xã Châu Pha, Huyện Tân Thành, Tỉnh HÀ GIANG",
   "Longtitude": 10.5737817,
   "Latitude": 107.1543601,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 10,
   "Name": "Trạm y tế Xã Tóc Tiên",
   "address": "Xã Tóc Tiên, Huyện Tân Thành, Tỉnh HÀ GIANG",
   "Longtitude": 10.6032018,
   "Latitude": 107.113007,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 11,
   "Name": "Trạm y tế Thị trấn Đất Đỏ",
   "address": "Thị trấn Đất Đỏ, Huyện Đất Đỏ, Tỉnh HÀ GIANG",
   "Longtitude": 10.4913475,
   "Latitude": 107.2725505,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 12,
   "Name": "Trạm y tế Xã Phước Long Thọ",
   "address": "Xã Phước Long Thọ, Huyện Đất Đỏ, Tỉnh HÀ GIANG",
   "Longtitude": 10.5059422,
   "Latitude": 107.3021069,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 13,
   "Name": "Trạm y tế Xã Phước Hội",
   "address": "Xã Phước Hội, Huyện Đất Đỏ, Tỉnh HÀ GIANG",
   "Longtitude": 10.4506553,
   "Latitude": 107.2824195,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 14,
   "Name": "Trạm y tế Xã Long Mỹ",
   "address": "Xã Long Mỹ, Huyện Đất Đỏ, Tỉnh HÀ GIANG",
   "Longtitude": 10.4372227,
   "Latitude": 107.2725505,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 15,
   "Name": "Trạm y tế Thị trấn Phước Hải",
   "address": "Thị trấn Phước Hải, Huyện Đất Đỏ, Tỉnh HÀ GIANG",
   "Longtitude": 10.4054168,
   "Latitude": 107.2607289,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 16,
   "Name": "Trạm y tế Xã Long Tân",
   "address": "Xã Long Tân, Huyện Đất Đỏ, Tỉnh HÀ GIANG",
   "Longtitude": 10.5505926,
   "Latitude": 107.2784615,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 17,
   "Name": "Trạm y tế Xã Láng Dài",
   "address": "Xã Láng Dài, Huyện Đất Đỏ, Tỉnh HÀ GIANG",
   "Longtitude": 10.5249522,
   "Latitude": 107.3494043,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 18,
   "Name": "Trạm y tế Xã Lộc An",
   "address": "Xã Lộc An, Huyện Đất Đỏ, Tỉnh HÀ GIANG",
   "Longtitude": 10.4765664,
   "Latitude": 107.3434916,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 19,
   "Name": "Trạm y tế Thị trấn Long Điền",
   "address": "Thị trấn Long Điền, Huyện Long Điền, Tỉnh HÀ GIANG",
   "Longtitude": 10.4812576,
   "Latitude": 107.2104935,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 20,
   "Name": "Trạm y tế Thị trấn Long Hải",
   "address": "Thị trấn Long Hải, Huyện Long Điền, Tỉnh HÀ GIANG",
   "Longtitude": 10.395902,
   "Latitude": 107.2370874,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 21,
   "Name": "Trạm y tế Xã An Ngãi",
   "address": "Xã An Ngãi, Huyện Long Điền, Tỉnh HÀ GIANG",
   "Longtitude": 10.4513168,
   "Latitude": 107.2134482,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 22,
   "Name": "Trạm y tế Xã Tam Phước",
   "address": "Xã Tam Phước, Huyện Long Điền, Tỉnh HÀ GIANG",
   "Longtitude": 10.4608351,
   "Latitude": 107.2370874,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 23,
   "Name": "Trạm y tế Xã An Nhứt",
   "address": "Xã An Nhứt, Huyện Long Điền, Tỉnh HÀ GIANG",
   "Longtitude": 10.4901197,
   "Latitude": 107.2459527,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 24,
   "Name": "Trạm y tế Xã Phước Tỉnh",
   "address": "Xã Phước Tỉnh, Huyện Long Điền, Tỉnh HÀ GIANG",
   "Longtitude": 10.4118663,
   "Latitude": 107.1927658,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 25,
   "Name": "Trạm y tế Xã Phước Hưng",
   "address": "Xã Phước Hưng, Huyện Long Điền, Tỉnh HÀ GIANG",
   "Longtitude": 10.4283624,
   "Latitude": 107.2370874,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 26,
   "Name": "Trạm y tế Thị trấn Phước Bửu",
   "address": "Thị trấn Phước Bửu, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.5353285,
   "Latitude": 107.4055814,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 27,
   "Name": "Trạm y tế Xã Phước Thuận",
   "address": "Xã Phước Thuận, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.4790005,
   "Latitude": 107.3967105,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 28,
   "Name": "Trạm y tế Xã Phước Tân",
   "address": "Xã Phước Tân, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.5669449,
   "Latitude": 107.3730563,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 29,
   "Name": "Trạm y tế Xã Xuyên Mộc",
   "address": "Xã Xuyên Mộc, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.5585237,
   "Latitude": 107.4262813,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 30,
   "Name": "Trạm y tế Xã Bông Trang",
   "address": "Xã Bông Trang, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.5355319,
   "Latitude": 107.4499404,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 31,
   "Name": "Trạm y tế Xã Tân Lâm",
   "address": "Xã Tân Lâm, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.7377268,
   "Latitude": 107.4203669,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 32,
   "Name": "Trạm y tế Xã Bàu Lâm",
   "address": "Xã Bàu Lâm, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.6970348,
   "Latitude": 107.3730563,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 33,
   "Name": "Trạm y tế Xã Hòa Bình",
   "address": "Xã Hòa Bình, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.6089467,
   "Latitude": 107.3967105,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 34,
   "Name": "Trạm y tế Xã Hòa Hưng",
   "address": "Xã Hòa Hưng, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.6573892,
   "Latitude": 107.4026244,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 35,
   "Name": "Trạm y tế Xã Hòa Hiệp",
   "address": "Xã Hòa Hiệp, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.6787414,
   "Latitude": 107.5031811,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 36,
   "Name": "Trạm y tế Xã Hòa Hội",
   "address": "Xã Hòa Hội, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.6279317,
   "Latitude": 107.4440254,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 37,
   "Name": "Trạm y tế Xã Bưng Riềng",
   "address": "Xã Bưng Riềng, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.538601,
   "Latitude": 107.4913489,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 38,
   "Name": "Trạm y tế Xã Bình Châu",
   "address": "Xã Bình Châu, Huyện Xuyên Mộc, Tỉnh HÀ GIANG",
   "Longtitude": 10.57921,
   "Latitude": 107.5386809,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 39,
   "Name": "Trạm y tế Xã Bàu Chinh",
   "address": "Xã Bàu Chinh, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.678295,
   "Latitude": 107.2252675,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 40,
   "Name": "Trạm y tế Thị trấn Ngãi Giao",
   "address": "Thị trấn Ngãi Giao, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.6559243,
   "Latitude": 107.2370874,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 41,
   "Name": "Trạm y tế Xã Bình Ba",
   "address": "Xã Bình Ba, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.6182884,
   "Latitude": 107.2311774,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 42,
   "Name": "Trạm y tế Xã Suối Nghệ",
   "address": "Xã Suối Nghệ, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.6043384,
   "Latitude": 107.1898113,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 43,
   "Name": "Trạm y tế Xã Xuân Sơn",
   "address": "Xã Xuân Sơn, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.6404024,
   "Latitude": 107.3198424,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 44,
   "Name": "Trạm y tế Xã Sơn Bình",
   "address": "Xã Sơn Bình, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.649907,
   "Latitude": 107.3434916,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 45,
   "Name": "Trạm y tế Xã Bình Giã",
   "address": "Xã Bình Giã, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.6437421,
   "Latitude": 107.2607289,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 46,
   "Name": "Trạm y tế Xã Bình Trung",
   "address": "Xã Bình Trung, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.6424077,
   "Latitude": 107.2843726,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 47,
   "Name": "Trạm y tế Xã Xà Bang",
   "address": "Xã Xà Bang, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.7319088,
   "Latitude": 107.2370874,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 48,
   "Name": "Trạm y tế Xã Cù Bị",
   "address": "Xã Cù Bị, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.7294922,
   "Latitude": 107.1839024,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 49,
   "Name": "Trạm y tế Xã Láng Lớn",
   "address": "Xã Láng Lớn, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.6592505,
   "Latitude": 107.1779936,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 50,
   "Name": "Trạm y tế Xã Quảng Thành",
   "address": "Xã Quảng Thành, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.694576,
   "Latitude": 107.2782896,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 51,
   "Name": "Trạm y tế Xã Kim Long",
   "address": "Xã Kim Long, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.7050987,
   "Latitude": 107.2311774,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 52,
   "Name": "Trạm y tế Xã Suối Rao",
   "address": "Xã Suối Rao, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.5912828,
   "Latitude": 107.3257545,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 53,
   "Name": "Trạm y tế Xã Đá Bạc",
   "address": "Xã Đá Bạc, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.5939448,
   "Latitude": 107.2784615,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 54,
   "Name": "Trạm y tế Xã Nghĩa Thành",
   "address": "Xã Nghĩa Thành, Huyện Châu Đức, Tỉnh HÀ GIANG",
   "Longtitude": 10.5718057,
   "Latitude": 107.1898113,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 55,
   "Name": "Trạm y tế Phường Phước Hưng",
   "address": "Phường Phước Hưng, Thành phố Bà Rịa, Tỉnh HÀ GIANG",
   "Longtitude": 10.5153943,
   "Latitude": 107.180948,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 56,
   "Name": "Trạm y tế Phường Phước Hiệp",
   "address": "Phường Phước Hiệp, Thành phố Bà Rịa, Tỉnh HÀ GIANG",
   "Longtitude": 10.4983607,
   "Latitude": 107.1706079,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 57,
   "Name": "Trạm y tế Phường Phước Nguyên",
   "address": "Phường Phước Nguyên, Thành phố Bà Rịa, Tỉnh HÀ GIANG",
   "Longtitude": 10.4991423,
   "Latitude": 107.180948,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 58,
   "Name": "Trạm y tế Phường Long Toàn",
   "address": "Phường Long Toàn, Thành phố Bà Rịa, Tỉnh HÀ GIANG",
   "Longtitude": 10.4916352,
   "Latitude": 107.194243,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 59,
   "Name": "Trạm y tế Phường Long Tâm",
   "address": "Phường Long Tâm, Thành phố Bà Rịa, Tỉnh HÀ GIANG",
   "Longtitude": 10.5035763,
   "Latitude": 107.1986748,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 60,
   "Name": "Trạm y tế Phường Phước Trung",
   "address": "Phường Phước Trung, Thành phố Bà Rịa, Tỉnh HÀ GIANG",
   "Longtitude": 10.4857647,
   "Latitude": 107.1779936,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 61,
   "Name": "Trạm y tế Phường Long Hương",
   "address": "Phường Long Hương, Thành phố Bà Rịa, Tỉnh HÀ GIANG",
   "Longtitude": 10.4950334,
   "Latitude": 107.1573142,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 62,
   "Name": "Trạm y tế Phường Kim Dinh",
   "address": "Phường Kim Dinh, Thành phố Bà Rịa, Tỉnh HÀ GIANG",
   "Longtitude": 10.4999931,
   "Latitude": 107.1375927,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 63,
   "Name": "Trạm y tế Xã Tân Hưng",
   "address": "Xã Tân Hưng, Thành phố Bà Rịa, Tỉnh HÀ GIANG",
   "Longtitude": 10.5319776,
   "Latitude": 107.1750393,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 64,
   "Name": "Trạm y tế Xã Long Phước",
   "address": "Xã Long Phước, Thành phố Bà Rịa, Tỉnh HÀ GIANG",
   "Longtitude": 10.5156408,
   "Latitude": 107.2252675,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 65,
   "Name": "Trạm y tế Xã Hoà Long",
   "address": "Xã Hoà Long, Thành phố Bà Rịa, Tỉnh HÀ GIANG",
   "Longtitude": 10.5277905,
   "Latitude": 107.2016295,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 66,
   "Name": "Trạm y tế Phường 1",
   "address": "Phường 1, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3522829,
   "Latitude": 107.0701844,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 67,
   "Name": "Trạm y tế Phường Thắng Tam",
   "address": "Phường Thắng Tam, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3434329,
   "Latitude": 107.0914726,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 68,
   "Name": "Trạm y tế Phường 2",
   "address": "Phường 2, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3314365,
   "Latitude": 107.0805202,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 69,
   "Name": "Trạm y tế Phường 3",
   "address": "Phường 3, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3489365,
   "Latitude": 107.0819968,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 70,
   "Name": "Trạm y tế Phường 4",
   "address": "Phường 4, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3545069,
   "Latitude": 107.0790436,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 71,
   "Name": "Trạm y tế Phường 5",
   "address": "Phường 5, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3756812,
   "Latitude": 107.062802,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 72,
   "Name": "Trạm y tế Phường Thắng Nhì",
   "address": "Phường Thắng Nhì, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3723385,
   "Latitude": 107.0723668,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 73,
   "Name": "Trạm y tế Phường 7",
   "address": "Phường 7, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3638951,
   "Latitude": 107.0805202,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 74,
   "Name": "Trạm y tế Phường Nguyễn An Ninh",
   "address": "Phường Nguyễn An Ninh, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3621958,
   "Latitude": 107.0949818,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 75,
   "Name": "Trạm y tế Phường 8",
   "address": "Phường 8, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3564094,
   "Latitude": 107.0938097,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 76,
   "Name": "Trạm y tế Phường 9",
   "address": "Phường 9, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3740741,
   "Latitude": 107.0923331,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 77,
   "Name": "Trạm y tế Phường Thắng Nhất",
   "address": "Phường Thắng Nhất, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3891035,
   "Latitude": 107.1016716,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 78,
   "Name": "Trạm y tế Phường Rạch Dừa",
   "address": "Phường Rạch Dừa, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.4009204,
   "Latitude": 107.1159559,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 79,
   "Name": "Trạm y tế Phường 10",
   "address": "Phường 10, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.3920828,
   "Latitude": 107.1120319,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 80,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.4017412,
   "Latitude": 107.1307289,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 81,
   "Name": "Trạm y tế Phường 12",
   "address": "Phường 12, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.4230641,
   "Latitude": 107.1582633,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 },
 {
   "STT": 82,
   "Name": "Trạm y tế Xã Long Sơn",
   "address": "Xã Long Sơn, Thành phố Vũng Tàu, Tỉnh HÀ GIANG",
   "Longtitude": 10.4532541,
   "Latitude": 107.0956304,
   "Rating": 3.98,
   "area": "Tỉnh",
   "Number_of_beds": 5
 }
];